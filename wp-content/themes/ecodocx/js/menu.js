jQuery(document).ready(function($){
    /*scroll menu*/
    var num = 400;
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > num) {
            $('header').addClass('menu-reduced');
        } else {
            $('header').removeClass('menu-reduced');
        }
    });

    /* Mobile menu */
    //$('.header-menu-small').hide();
    $('#burger-menu').on('click', function(){
        $('.header-menu-small').slideToggle();

    });

    $('.header-menu-small > .nav-menu-small > .menu-item-2412 > a').on('click', function(){
        $('.header-menu-small > .nav-menu-small > .menu-item-2412 > .ul-wrap').slideToggle();
    });

    $('.header-menu-small > .nav-menu-small > .menu-item-2411 > a').on('click', function(){
        $('.header-menu-small > .nav-menu-small > .menu-item-2411 > .ul-wrap').slideToggle();
    });

    $('.header-menu-small > .nav-menu-small > .menu-item-2412 > .ul-wrap > .sub-menu > .menu-item-2415 > a').on('click', function(){
        $('.header-menu-small > .nav-menu-small > .menu-item-2412 > .ul-wrap > .sub-menu > .menu-item-2415 > .ul-wrap').slideToggle();
    });

    $('.header-menu-small > .nav-menu-small > .menu-item-2412 > .ul-wrap > .sub-menu > .menu-item-2416 > a').on('click', function(){
        $('.header-menu-small > .nav-menu-small > .menu-item-2412 > .ul-wrap > .sub-menu > .menu-item-2416 > .ul-wrap').slideToggle();
    });

    $('.header-menu-small > .nav-menu-small > .menu-item-2412 > .ul-wrap > .sub-menu > .menu-item-2417 > a').on('click', function(){
        $('.header-menu-small > .nav-menu-small > .menu-item-2412 > .ul-wrap > .sub-menu > .menu-item-2417 > .ul-wrap').slideToggle();
    });

    $('.header-menu-small > .nav-menu-small > .menu-item-2412 > .ul-wrap > .sub-menu > .menu-item-2418 > a').on('click', function(){
        $('.header-menu-small > .nav-menu-small > .menu-item-2412 > .ul-wrap > .sub-menu > .menu-item-2418 > .ul-wrap').slideToggle();
    });

    $('.header-menu-small > .nav-menu-small > .menu-item-2412 > .ul-wrap > .sub-menu > .menu-item-2418 > .ul-wrap > .sub-menu > .menu-item-2419 > a').on('click', function(){
        $('.header-menu-small > .nav-menu-small > .menu-item-2412 > .ul-wrap > .sub-menu > .menu-item-2418 > .ul-wrap > .sub-menu  .menu-item-2419 > .ul-wrap').slideToggle();
    });

    $('.header-menu-small > .nav-menu-small > .menu-item-2412 > .ul-wrap > .sub-menu > .menu-item-2418 > .ul-wrap > .sub-menu > .menu-item-2420 > a').on('click', function(){
        $('.header-menu-small > .nav-menu-small > .menu-item-2412 > .ul-wrap > .sub-menu > .menu-item-2418 > .ul-wrap > .sub-menu  .menu-item-2420 > .ul-wrap').slideToggle();
    });

    $('.header-menu-small > .nav-menu-small > .menu-item-2412 > .ul-wrap > .sub-menu > .menu-item-2418 > .ul-wrap > .sub-menu > .menu-item-2421 > a').on('click', function(){
        $('.header-menu-small > .nav-menu-small > .menu-item-2412 > .ul-wrap > .sub-menu > .menu-item-2418 > .ul-wrap > .sub-menu  .menu-item-2421 > .ul-wrap').slideToggle();
    });

    $('.header-menu-small-language > .nav-menu-small-language > .menu-item-2436 > a').on('click', function(){
        $('.header-menu-small-language > .nav-menu-small-language > .menu-item-2436 > .ul-wrap').slideToggle();
    });


    /*$('.header-menu-small > .nav-menu-small > li').hide();
     $('.header-menu-small > .nav-menu-small > li > .ul-wrap').on('click', function(){
         $(" .ul-wrap").slideToggle();
     });*/

    /*$('li.language').hover(function() {
            $('#menu-language-menu').toggleClass('active');
        }
    );*/

    /*setTimeout menu*/
    $('.nav-menu > li').hover(function () {
        clearTimeout($.data(this,'timer'));
        $('> .ul-wrap',this).stop(true,true).slideDown(50);
    },  function () {
        $.data(this,'timer', setTimeout($.proxy(function() {
            $('> .ul-wrap',this).stop(true,true).slideUp(50);
        }, this), 250));
    } );

    $('.header-menu-language > ul > li').hover(function () {
        clearTimeout($.data(this,'timer'));
        $('> .ul-wrap',this).stop(true,true).slideDown(50);
    },  function () {
        $.data(this,'timer', setTimeout($.proxy(function() {
            $('> .ul-wrap',this).stop(true,true).slideUp(50);
        }, this), 250));
    } );
  /*  $('.nav-menu > li').hover(function() {
            $('> .ul-wrap > ul > li > .ul-wrap:first', this).show().addClass('first');
        }
    );*/
    $('.nav-menu > li').hover(function() {
            $('> .ul-wrap > ul > li:first', this).addClass('first');
        }
    );
    /*$('.nav-menu li .ul-wrap ul li').hover(function() {
            $(this).sibling().removeClass('first').hide();
            //$(this).toggleClass('first sub-active');
        }
    );*/
    $('.nav-menu li .ul-wrap ul li').hover(function() {
            $(this).toggleClass('sub-active');
        }
    );

    /*$('.nav-menu > li').mouseleave(function() {
            $('> .ul-wrap > ul > li > .ul-wrap:first', this).removeClass('first');
        }
    );*/
/*    $('.nav-menu > li > .ul-wrap > ul > li').hover(function() {
            $('> .ul-wrap', this).toggleClass('sub-active').addClass('first').show();
        }, function() {
            $('> .ul-wrap', this).sibling('.sub-active').removeClass('first').hide();
        }
    );*/

    /*Scroll btn*/
    $('#scroll-up').on('click', function() {
        $('html, body').animate({scrollTop: 0}, 900);
    });
});


$(window).scroll(function() {
    var mainParallax = $(this).scrollTop();
    $('.main-header-content').css({
        "transform" : "translate(0%, " + mainParallax /3 + "%"
    });
});

