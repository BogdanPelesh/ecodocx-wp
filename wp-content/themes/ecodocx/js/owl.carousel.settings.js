$(document).ready(function() {
    $("#slider").owlCarousel({
        loop: true,
        items: 1,
        nav: false,
        dots: true,
        autoplay:true,
        smartSpeed:1000,
        autoplayTimeout: 7000,
        autoplayHoverPause:true
    });
   $(".single-review").owlCarousel({
        loop: true,
        items: 1,
        nav: false,
        autoplay:true,
        smartSpeed:1000,
        autoplayTimeout: 7000,
        dots: false,
        navText: ["<img class=\"prev\" src=\"/wp-content/uploads/2017/09/slider-left-control.png\">","<img class=\"next\" src=\"/wp-content/uploads/2017/09/slider-right-control.png\">"],
        responsive:{
            768:{
                nav: true
            }
        }
    });
    $(".related-products-wrap").owlCarousel({
        loop: true,
        nav: true,
        dots: false,
        autoplay:true,
        smartSpeed: 1000,
        autoplayTimeout:3000,
        responsiveClass:true,
        autoplayHoverPause:true,
        responsive:{
            425: {
                items:1
            },
            768: {
                items:2
            },
            1200:{
                items:3
            }
        },
        navText: ["<i class=\"fa fa-angle-left prev\"></i>","<i class=\"fa fa-angle-right next\"></i>"]
    });
});
