<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package ecodocx
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				<div class="container">
                    <div class="row">
                        <div class="col-md-offset-1 col-xs-12 col-md-6">
                            <div class="content-error">
                                <h1>Oops!</h1>
                                <h3>We can't seem to find the page you're looking for.</h3>
                                <p>Error code: 404</p>
                                <p>Please check your URL, or return to the <a href="/">Home page</a></p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-5">
                            <img src="/wp-content/uploads/2017/09/404-error.gif" alt="404-error">
                        </div>
                    </div>
                </div>
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
