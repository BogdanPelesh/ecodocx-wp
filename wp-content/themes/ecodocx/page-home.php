<?php
/*
Template Name: Page Home
*/
?>

<?php if (get_bloginfo('language')=="de-DE") { ?>
	<?php get_header('de'); ?>
    <section id="slider" class="owl-carousel">
        <div class="single-slide" style="background: url('<?php echo get_theme_mod('first_featured_img_de', ''); ?>'); background-size: cover;">
            <div class="slider-content">
                <div class="container">
                    <h1><?php echo get_theme_mod('first_title_de', ''); ?></h1>
                    <p class="text-slider"><?php echo get_theme_mod('first_content_de', ''); ?></p>
                    <a class="btn-info" href="<?php echo get_theme_mod('first_button_de', ''); ?>">Erfahren Sie mehr</a></div>
            </div>
        </div>
        <div class="single-slide" style="background: url('<?php echo get_theme_mod('second_featured_img_de', ''); ?>'); background-size: cover;">
            <div class="slider-content">
                <div class="container">
                    <h1><?php echo get_theme_mod('second_title_de', ''); ?></h1>
                    <p class="text-slider"><?php echo get_theme_mod('second_content_de', ''); ?></p>
                    <a class="btn-info" href="<?php echo get_theme_mod('second_button_de', ''); ?>">Erfahren Sie mehr</a></div>
            </div>
        </div>
        <div class="single-slide" style="background: url('<?php echo get_theme_mod('third_featured_img_de', ''); ?>'); background-size: cover;">
            <div class="slider-content">
                <div class="container">
                    <h1><?php echo get_theme_mod('third_title_de', ''); ?></h1>
                    <p class="text-slider"><?php echo get_theme_mod('third_content_de', ''); ?></p>
                    <a class="btn-info" href="<?php echo get_theme_mod('third_button_de', ''); ?>">Erfahren Sie mehr</a></div>
            </div>
        </div>
        <div class="single-slide" style="background: url('<?php echo get_theme_mod('fourth_featured_img_de', ''); ?>'); background-size: cover;">
            <div class="slider-content">
                <div class="container">
                    <h1><?php echo get_theme_mod('fourth_title_de', ''); ?></h1>
                    <p class="text-slider"><?php echo get_theme_mod('fourth_content_de', ''); ?></p>
                    <a class="btn-info" href="<?php echo get_theme_mod('fourth_button_de', ''); ?>">Erfahren Sie mehr</a></div>
            </div>
        </div>
    </section>
<?php } else if (get_bloginfo('language')=="en-US") { ?>
	<?php get_header(); ?>
        <section id="slider" class="owl-carousel">
            <div class="single-slide" style="background: url('<?php echo get_theme_mod('first_featured_img_en', ''); ?>'); background-size: cover;">
                <div class="slider-content">
                    <div class="container">
                        <h1><?php echo get_theme_mod('first_title_en', ''); ?></h1>
                        <p class="text-slider"><?php echo get_theme_mod('first_content_en', ''); ?></p>
                        <a class="btn-info" href="<?php echo get_theme_mod('first_button_en', ''); ?>">Learn more</a></div>
                </div>
            </div>
            <div class="single-slide" style="background: url('<?php echo get_theme_mod('second_featured_img_en', ''); ?>'); background-size: cover;">
                <div class="slider-content">
                    <div class="container">
                        <h1><?php echo get_theme_mod('second_title_en', ''); ?></h1>
                        <p class="text-slider"><?php echo get_theme_mod('second_content_en', ''); ?></p>
                        <a class="btn-info" href="<?php echo get_theme_mod('second_button_en', ''); ?>">Learn more</a></div>
                </div>
            </div>
            <div class="single-slide" style="background: url('<?php echo get_theme_mod('third_featured_img_en', ''); ?>'); background-size: cover;">
                <div class="slider-content">
                    <div class="container">
                        <h1><?php echo get_theme_mod('third_title_en', ''); ?></h1>
                        <p class="text-slider"><?php echo get_theme_mod('third_content_en', ''); ?></p>
                        <a class="btn-info" href="<?php echo get_theme_mod('third_button_en', ''); ?>">Learn more</a></div>
                </div>
            </div>
            <div class="single-slide" style="background: url('<?php echo get_theme_mod('fourth_featured_img_en', ''); ?>'); background-size: cover;">
                <div class="slider-content">
                    <div class="container">
                        <h1><?php echo get_theme_mod('fourth_title_en', ''); ?></h1>
                        <p class="text-slider"><?php echo get_theme_mod('fourth_content_en', ''); ?></p>
                        <a class="btn-info" href="<?php echo get_theme_mod('fourth_button_en', ''); ?>">Learn more</a></div>
                </div>
            </div>
        </section>
<?php } ?>

    <div id="markups">
        <a id="scroll-up"><i class="fa fa-arrow-up"></i></a>
        <a id="contact-us" href=call:+16174751636"><i class="fa fa-phone"></i></a>
        <a target="_blank" rel="nofollow" href="https://www.linkedin.com/company-beta/3565752/"><i class="fa fa-linkedin"></i></a>
        <a target="_blank" rel="nofollow" href="https://twitter.com/ecodocx/"><i class="fa fa-twitter"></i></a>
        <a target="_blank" rel="nofollow" href="https://www.facebook.com/ecodocx/"><i class="fa fa-facebook"></i></a>
        <a rel="nofollow" href="mailto:sales@ecodocx.com"><i class="fa fa-envelope"></i></a>
    </div>

<?php the_post(); ?>
<?php the_content(); ?>

<?php if (get_bloginfo('language')=="de-DE") { ?>
    <section id="reviews">
        <div class="container">
            <h2>Was unsere Kunden sagen:</h2>
            <div class="single-review owl-carousel">
                <div class="single-review-cell">
                    <p class="review-comment">“<?php echo get_theme_mod('first_review_comment_de', ''); ?>”</p>
                    <div class="author">
                        <p><?php echo get_theme_mod('first_review_companyname_de', ''); ?></p>
                    </div>
                </div>
                <div class="single-review-cell">
                    <p class="review-comment">“<?php echo get_theme_mod('second_review_comment_de', ''); ?>”</p>
                    <div class="author">
                        <p><?php echo get_theme_mod('second_review_companyname_de', ''); ?></p>
                    </div>
                </div>
                <div class="single-review-cell">
                    <p class="review-comment">“<?php echo get_theme_mod('third_review_comment_de', ''); ?>”</p>
                    <div class="author">
                        <p><?php echo get_theme_mod('third_review_companyname_de', ''); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
	<?php get_footer('de'); ?>
<?php } else if (get_bloginfo('language')=="en-US") { ?>
        <section id="reviews">
            <div class="container">
                <h2>What our customers say</h2>
                <div class="single-review owl-carousel">
                    <div class="single-review-cell">
                        <p class="review-comment">“<?php echo get_theme_mod('first_review_comment_en', ''); ?>”</p>
                        <div class="author">
                            <p><?php echo get_theme_mod('first_review_companyname_en', ''); ?></p>
                        </div>
                    </div>
                    <div class="single-review-cell">
                        <p class="review-comment">“<?php echo get_theme_mod('second_review_comment_en', ''); ?>”</p>
                        <div class="author">
                            <p><?php echo get_theme_mod('second_review_companyname_en', ''); ?></p>
                        </div>
                    </div>
                    <div class="single-review-cell">
                        <p class="review-comment">“<?php echo get_theme_mod('third_review_comment_en', ''); ?>”</p>
                        <div class="author">
                            <p><?php echo get_theme_mod('third_review_companyname_en', ''); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
	<?php get_footer(); ?>
<?php } ?>