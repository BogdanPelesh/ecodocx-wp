<footer>
    <div class="info">
        <div class="container">
            <div class="row">
                <div class="intro col-md-4 col-sm-6">
                    <p><b><span>Ecodocx</span></b> entwickelt effektive und intelligente Lösungen, die Unternehmen bei der Optimierung ihrer Kundenkommunikation, Dokumenten- und Content-Management unterstützen.</p>
                </div>
                <div class="questions col-md-4 col-sm-6">
                    <a href="/contact-de/" class="btn-info">Stellen Sie unseren Experten eine Frage</a>
                    <p>Zögern Sie nicht uns zu kontaktieren. Jede E-Mail oder Anruf wird beantwortet.</p>
                </div>
                <div class="contact-us col-md-2 col-sm-6">
                    <h4>Kontaktieren Sie Ecodocx</h4>
                    <p>
                        <a class="phone" href="tel:+16174751636">+1 617-475-1636</a><br />
                        <a class="mail" href="mailto:sales@ecodocx.com">sales@ecodocx.com</a>
                    </p>
                </div>
                <div class="social col-md-2 col-sm-6">
                    <h4>Folgen Sie uns auf:</h4>
                    <ul>
                        <li><a target="_blank" href="https://twitter.com/ecodocx/"><i class="fa fa-twitter"></i></a></li>
                        <li><a target="_blank" href="https://www.facebook.com/ecodocx/"><i class="fa fa-facebook"></i></a></li>
                        <li><a target="_blank" href="https://www.linkedin.com/company-beta/3565752/"><i class="fa fa-linkedin-square"></i></a></li>
                        <li><a target="_blank" href="https://www.youtube.com/channel/UC_w-dEaPROBQEehWP0aIZMA"><i class="fa fa-youtube-play"></i></a></li>
                        <li><a target="_blank" href="https://www.xing.com/companies/ecodocx"><i class="fa fa-xing" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-3"><a href="<?php echo home_url(); ?>"><img src="/wp-content/uploads/2017/08/logo-footer.png" alt="Ecodocx"></a></div>
                <div class="col-sm-9">
                    <p>&#169; 2009-<?php echo Date('Y'); ?> Ecodocx llc. All rights reserved. <a href="/privacy-policy/">Privacy policy</a> <span>|</span> <a href="/imprint-de/">Imprint</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
<script>
    $('.col-eq').colequalizer();

    window.sr = ScrollReveal({ reset: false });
    sr.reveal('.blink', { origin: 'bottom', duration: 800, delay: 300, opacity: 0, distance: '0' });
    sr.reveal('.anBottom', { origin: 'bottom', duration: 500, delay: 200, opacity: 0, distance: '50px'}, 200);
    sr.reveal('.anBottom-ind', { origin: 'bottom', duration: 500, delay: 200, opacity: 0, distance: '50px'}, 200);
    sr.reveal('.anLeft', { origin: 'left',  duration: 1200, delay: 150, opacity: 0, distance: '180px'});
    sr.reveal('.anLeft-ind', { origin: 'left',  duration: 500, delay: 200, opacity: 0, distance: '180px'}, 200);
    sr.reveal('.anRight', { origin: 'right',  duration: 1200, delay: 250, opacity: 0, distance: '600px'});
</script>
</body>
</html>
