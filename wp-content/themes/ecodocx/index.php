<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ecodocx
 */

get_header(); ?>
    <div id="primary" class="posts content-area">
        <main id="main" class="site-main">
            <div class="container">
                <div class="row">
					<?php
					if ( have_posts() ) :

						if ( is_home() && ! is_front_page() ) : ?>
                            <div class="col-xs-12">
                                <h2 class="page-title">Ecodocx Enterprise Business Blog</h2>
                            </div>
							<div class="col-xs-12">
                                <ul class="categories-wrap all">
                                    <?php wp_list_categories( array(
                                        'show_option_all'    => 'All',
                                        'show_option_none'   => __('No categories'),
                                        'orderby'            => 'name',
                                        'order'              => 'ASC',
                                        'show_last_update'   => 0,
                                        'style'              => 'list',
                                        'show_count'         => 0,
                                        'hide_empty'         => 1,
                                        'use_desc_for_title' => 1,
                                        'child_of'           => 0,
                                        'feed'               => '',
                                        'feed_type'          => '',
                                        'feed_image'         => '',
                                        'exclude'            => '',
                                        'exclude_tree'       => '',
                                        'include'            => '',
                                        'hierarchical'       => true,
                                        'number'             => NULL,
                                        'echo'               => 1,
                                        'depth'              => 0,
                                        'current_category'   => 0,
                                        'pad_counts'         => 0,
                                        'taxonomy'           => 'category',
                                        'walker'             => 'Walker_Category',
                                        'hide_title_if_empty' => false,
                                        'separator'          => '',
                                    )); ?>
                                </ul>
                            </div>
							<?php
						endif;

						/* Start the Loop */
						while ( have_posts() ) : the_post();$postcounter++;

							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'template-parts/content', get_post_format() );
                        ?>
                            <?php if ($postcounter % 2 == 0) : ?>
                                <div class="clearfix visible-sm"></div>
                            <?php endif; ?>
							<?php if ($postcounter % 3 == 0) : ?>
                                <div class="clearfix visible-lg"></div>
							<?php endif; ?>
						<?php endwhile; ?>
                        <div class="col-xs-12">
                            <div class="paginate-blog">
                                <?php echo paginate_links(array (
	                                'prev_text'    => __('<i class="fa fa-long-arrow-left" aria-hidden="true"></i>'),
	                                'next_text'    => __('<i class="fa fa-long-arrow-right" aria-hidden="true"></i>'),
                                )); ?>
                            </div>
                        </div>
					<?php else :

						get_template_part( 'template-parts/content', 'none' );

					endif; ?>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->
<?php
//get_sidebar();
get_footer();
