<?php
/**
 * ecodocx Theme Customizer
 *
 * @package ecodocx
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function ecodocx_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'ecodocx_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'ecodocx_customize_partial_blogdescription',
		) );
	}
}
add_action( 'customize_register', 'ecodocx_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function ecodocx_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function ecodocx_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function ecodocx_customize_preview_js() {
	wp_enqueue_script( 'ecodocx-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'ecodocx_customize_preview_js' );


/*Home Slider En*/
add_action('customize_register', function($customizer){
	$customizer->add_section(
		'home_slider_en',
		array (
			'title' => 'Home slider En',
			'priority' => '2'
		)
	);

	/* First Home Slide En */
	$customizer->add_setting(
		'first_featured_img_en',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		)
	);
	$customizer->add_control(
		new WP_Customize_Image_Control( $customizer, 'first_featured_img_en',
			array (
				'label' => __( 'First image', 'theme-ecodocx' ),
				'section' => 'home_slider_en',
				'settings' => 'first_featured_img_en',
			)
		)
	);
	$customizer->add_setting(
		'first_title_en',
		array('default' => 'Title')
	);
	$customizer->add_control(
		'first_title_en',
		array(
			'label' => 'First title',
			'section' => 'home_slider_en',
			'type' => 'text',
		)
	);

	$customizer->add_setting(
		'first_content_en',
		array('default' => 'Text')
	);
	$customizer->add_control(
		'first_content_en',
		array(
			'label' => 'First content',
			'section' => 'home_slider_en',
			'type' => 'textarea',
		)
	);
	$customizer->add_setting(
		'first_button_en',
		array('default' => 'Title')
	);
	$customizer->add_control(
		'first_button_en',
		array(
			'label' => 'First button',
			'section' => 'home_slider_en',
			'type' => 'text',
		)
	);

	/* Second Home Slide */
	$customizer->add_setting(
		'second_featured_img_en',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		)
	);
	$customizer->add_control(
		new WP_Customize_Image_Control( $customizer, 'second_featured_img_en',
			array (
				'label' => __( 'Second image', 'theme-ecodocx' ),
				'section' => 'home_slider_en',
				'settings' => 'second_featured_img_en',
			)
		)
	);
	$customizer->add_setting(
		'second_title_en',
		array('default' => 'Title')
	);
	$customizer->add_control(
		'second_title_en',
		array(
			'label' => 'Second title',
			'section' => 'home_slider_en',
			'type' => 'text',
		)
	);
	$customizer->add_setting(
		'second_content_en',
		array('default' => 'Text')
	);
	$customizer->add_control(
		'second_content_en',
		array(
			'label' => 'Second content',
			'section' => 'home_slider_en',
			'type' => 'textarea',
		)
	);
	$customizer->add_setting(
		'second_button_en',
		array('default' => 'Title')
	);
	$customizer->add_control(
		'second_button_en',
		array(
			'label' => 'Second button',
			'section' => 'home_slider_en',
			'type' => 'text',
		)
	);

	/* Third Home Slide  */
	$customizer->add_setting(
		'third_featured_img_en',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		)
	);
	$customizer->add_control(
		new WP_Customize_Image_Control( $customizer, 'third_featured_img_en',
			array (
				'label' => __( 'Third image', 'theme-ecodocx' ),
				'section' => 'home_slider_en',
				'settings' => 'third_featured_img_en',
			)
		)
	);
	$customizer->add_setting(
		'third_title_en',
		array('default' => 'Title')
	);
	$customizer->add_control(
		'third_title_en',
		array(
			'label' => 'Third title',
			'section' => 'home_slider_en',
			'type' => 'text',
		)
	);
	$customizer->add_setting(
		'third_content_en',
		array('default' => 'Text')
	);
	$customizer->add_control(
		'third_content_en',
		array(
			'label' => 'Third content',
			'section' => 'home_slider_en',
			'type' => 'textarea',
		)
	);
	$customizer->add_setting(
		'third_button_en',
		array('default' => 'Title')
	);
	$customizer->add_control(
		'third_button_en',
		array(
			'label' => 'Third button',
			'section' => 'home_slider_en',
			'type' => 'text',
		)
	);

	/* Fourth Home Slide  */
	$customizer->add_setting(
		'fourth_featured_img_en',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		)
	);
	$customizer->add_control(
		new WP_Customize_Image_Control( $customizer, 'fourth_featured_img_en',
			array (
				'label' => __( 'Fourth image', 'theme-ecodocx' ),
				'section' => 'home_slider_en',
				'settings' => 'fourth_featured_img_en',
			)
		)
	);
	$customizer->add_setting(
		'fourth_title_en',
		array('default' => 'Title')
	);
	$customizer->add_control(
		'fourth_title_en',
		array(
			'label' => 'Fourth title',
			'section' => 'home_slider_en',
			'type' => 'text',
		)
	);
	$customizer->add_setting(
		'fourth_content_en',
		array('default' => 'Text')
	);
	$customizer->add_control(
		'fourth_content_en',
		array(
			'label' => 'Fourth content',
			'section' => 'home_slider_en',
			'type' => 'textarea',
		)
	);
	$customizer->add_setting(
		'fourth_button_en',
		array('default' => 'Title')
	);
	$customizer->add_control(
		'fourth_button_en',
		array(
			'label' => 'Fourth button',
			'section' => 'home_slider_en',
			'type' => 'text',
		)
	);
});

/*Home Slider De*/
add_action('customize_register', function($customizer){
	$customizer->add_section(
		'home_slider_de',
		array (
			'title' => 'Home slider De',
			'priority' => '2'
		)
	);

	/* First Home Slide De */
	$customizer->add_setting(
		'first_featured_img_de',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		)
	);
	$customizer->add_control(
		new WP_Customize_Image_Control( $customizer, 'first_featured_img_de',
			array (
				'label' => __( 'First image', 'theme-ecodocx' ),
				'section' => 'home_slider_de',
				'settings' => 'first_featured_img_de',
			)
		)
	);
	$customizer->add_setting(
		'first_title_de',
		array('default' => 'Title')
	);
	$customizer->add_control(
		'first_title_de',
		array(
			'label' => 'First title',
			'section' => 'home_slider_de',
			'type' => 'text',
		)
	);

	$customizer->add_setting(
		'first_content_de',
		array('default' => 'Text')
	);
	$customizer->add_control(
		'first_content_de',
		array(
			'label' => 'First content',
			'section' => 'home_slider_de',
			'type' => 'textarea',
		)
	);
	$customizer->add_setting(
		'first_button_de',
		array('default' => 'Title')
	);
	$customizer->add_control(
		'first_button_de',
		array(
			'label' => 'First button',
			'section' => 'home_slider_de',
			'type' => 'text',
		)
	);

	/* Second Home Slide */
	$customizer->add_setting(
		'second_featured_img_de',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		)
	);
	$customizer->add_control(
		new WP_Customize_Image_Control( $customizer, 'second_featured_img_de',
			array (
				'label' => __( 'Second image', 'theme-ecodocx' ),
				'section' => 'home_slider_de',
				'settings' => 'second_featured_img_de',
			)
		)
	);
	$customizer->add_setting(
		'second_title_de',
		array('default' => 'Title')
	);
	$customizer->add_control(
		'second_title_de',
		array(
			'label' => 'Second title',
			'section' => 'home_slider_de',
			'type' => 'text',
		)
	);
	$customizer->add_setting(
		'second_content_de',
		array('default' => 'Text')
	);
	$customizer->add_control(
		'second_content_de',
		array(
			'label' => 'Second content',
			'section' => 'home_slider_de',
			'type' => 'textarea',
		)
	);
	$customizer->add_setting(
		'second_button_de',
		array('default' => 'Title')
	);
	$customizer->add_control(
		'second_button_de',
		array(
			'label' => 'Second button',
			'section' => 'home_slider_de',
			'type' => 'text',
		)
	);

	/* Third Home Slide  */
	$customizer->add_setting(
		'third_featured_img_de',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		)
	);
	$customizer->add_control(
		new WP_Customize_Image_Control( $customizer, 'third_featured_img_de',
			array (
				'label' => __( 'Third image', 'theme-ecodocx' ),
				'section' => 'home_slider_de',
				'settings' => 'third_featured_img_de',
			)
		)
	);
	$customizer->add_setting(
		'third_title_de',
		array('default' => 'Title')
	);
	$customizer->add_control(
		'third_title_de',
		array(
			'label' => 'Third title',
			'section' => 'home_slider_de',
			'type' => 'text',
		)
	);
	$customizer->add_setting(
		'third_content_de',
		array('default' => 'Text')
	);
	$customizer->add_control(
		'third_content_de',
		array(
			'label' => 'Third content',
			'section' => 'home_slider_de',
			'type' => 'textarea',
		)
	);
	$customizer->add_setting(
		'third_button_de',
		array('default' => 'Title')
	);
	$customizer->add_control(
		'third_button_de',
		array(
			'label' => 'Third button',
			'section' => 'home_slider_de',
			'type' => 'text',
		)
	);

	/* Fourth Home Slide  */
	$customizer->add_setting(
		'fourth_featured_img_de',
		array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		)
	);
	$customizer->add_control(
		new WP_Customize_Image_Control( $customizer, 'fourth_featured_img_de',
			array (
				'label' => __( 'Fourth image', 'theme-ecodocx' ),
				'section' => 'home_slider_de',
				'settings' => 'fourth_featured_img_de',
			)
		)
	);
	$customizer->add_setting(
		'fourth_title_de',
		array('default' => 'Title')
	);
	$customizer->add_control(
		'fourth_title_de',
		array(
			'label' => 'Fourth title',
			'section' => 'home_slider_de',
			'type' => 'text',
		)
	);
	$customizer->add_setting(
		'fourth_content_de',
		array('default' => 'Text')
	);
	$customizer->add_control(
		'fourth_content_de',
		array(
			'label' => 'Fourth content',
			'section' => 'home_slider_de',
			'type' => 'textarea',
		)
	);
	$customizer->add_setting(
		'fourth_button_de',
		array('default' => 'Title')
	);
	$customizer->add_control(
		'fourth_button_de',
		array(
			'label' => 'Fourth button',
			'section' => 'home_slider_de',
			'type' => 'text',
		)
	);
});

/*Reviews slider En*/
add_action('customize_register', function($customizer){
	$customizer->add_section(
		'reviews_slider_en',
		array (
			'title' => 'Reviews slider En',
			'priority' => '3'
		)
	);

	/* First Review Slide */
	$customizer->add_setting(
		'first_review_comment_en',
		array('default' => 'First comment')
	);
	$customizer->add_control(
		'first_review_comment_en',
		array(
			'label' => 'First comment',
			'section' => 'reviews_slider_en',
			'type' => 'textarea',
		)
	);
	$customizer->add_setting(
		'first_review_companyname_en',
		array('default' => 'First company name')
	);
	$customizer->add_control(
		'first_review_companyname_en',
		array(
			'label' => 'First company name',
			'section' => 'reviews_slider_en',
			'type' => 'text',
		)
	);

	/* Second Review Slide  */
	$customizer->add_setting(
		'second_review_comment_en',
		array('default' => 'Second comment')
	);
	$customizer->add_control(
		'second_review_comment_en',
		array(
			'label' => 'Second comment',
			'section' => 'reviews_slider_en',
			'type' => 'textarea',
		)
	);
	$customizer->add_setting(
		'second_review_companyname_en',
		array('default' => 'Second company')
	);
	$customizer->add_control(
		'second_review_companyname_en',
		array(
			'label' => 'Second company',
			'section' => 'reviews_slider_en',
			'type' => 'text',
		)
	);

	/* Third Review Slide */
	$customizer->add_setting(
		'third_review_comment_en',
		array('default' => 'Third comment')
	);
	$customizer->add_control(
		'third_review_comment_en',
		array(
			'label' => 'Third comment',
			'section' => 'reviews_slider_en',
			'type' => 'textarea',
		)
	);
	$customizer->add_setting(
		'third_review_companyname_en',
		array('default' => 'Third company name')
	);
	$customizer->add_control(
		'third_review_companyname_en',
		array(
			'label' => 'Third company name',
			'section' => 'reviews_slider_en',
			'type' => 'text',
		)
	);
});

/*Reviews slider De*/
add_action('customize_register', function($customizer){
	$customizer->add_section(
		'reviews_slider_de',
		array (
			'title' => 'Reviews slider De',
			'priority' => '4'
		)
	);

	/* First Review Slide */
	$customizer->add_setting(
		'first_review_comment_de',
		array('default' => 'First comment')
	);
	$customizer->add_control(
		'first_review_comment_de',
		array(
			'label' => 'First comment',
			'section' => 'reviews_slider_de',
			'type' => 'textarea',
		)
	);
	$customizer->add_setting(
		'first_review_companyname_de',
		array('default' => 'First company name')
	);
	$customizer->add_control(
		'first_review_companyname_de',
		array(
			'label' => 'First company name',
			'section' => 'reviews_slider_de',
			'type' => 'text',
		)
	);

	/* Second Review Slide  */
	$customizer->add_setting(
		'second_review_comment_de',
		array('default' => 'Second comment')
	);
	$customizer->add_control(
		'second_review_comment_de',
		array(
			'label' => 'Second comment',
			'section' => 'reviews_slider_de',
			'type' => 'textarea',
		)
	);
	$customizer->add_setting(
		'second_review_companyname_de',
		array('default' => 'Second company')
	);
	$customizer->add_control(
		'second_review_companyname_de',
		array(
			'label' => 'Second company',
			'section' => 'reviews_slider_de',
			'type' => 'text',
		)
	);

	/* Third Review Slide */
	$customizer->add_setting(
		'third_review_comment_de',
		array('default' => 'Third comment')
	);
	$customizer->add_control(
		'third_review_comment_de',
		array(
			'label' => 'Third comment',
			'section' => 'reviews_slider_de',
			'type' => 'textarea',
		)
	);
	$customizer->add_setting(
		'third_review_companyname_de',
		array('default' => 'Third company name')
	);
	$customizer->add_control(
		'third_review_companyname_de',
		array(
			'label' => 'Third company name',
			'section' => 'reviews_slider_de',
			'type' => 'text',
		)
	);
});