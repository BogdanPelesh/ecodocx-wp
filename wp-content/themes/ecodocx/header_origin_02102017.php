<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <title><?php wp_title(); ?></title>
	<?php wp_head(); ?>
</head>
<body>
<header class="<?php if ( is_user_logged_in() && is_admin_bar_showing() ) { echo 'logged-in'; } ?>">
    <div class="container">
        <div class="logo">
            <a href="<?php echo home_url(); ?>">
                <?php the_custom_logo(); ?>
            </a>
        </div>
        <div id="burger-menu">
            <i class="fa fa-bars"></i>
        </div>
        <?php
        wp_nav_menu( array(
		        'menu'              => 'ecodocx-menu-small-language',
		        'theme_location'    => 'primary',
		        'depth'             => 5,
		        'container'         => 'div',
		        'container_class'   => 'header-menu-small-language',
		        'container_id'      => '',
		        'menu_class'        => 'nav-menu-small-language',
		        'fallback_cb'       => '',
		        'walker'            => ''
	        )
        );
        ?>
        <nav class="page-nav">
	        <?php
	        wp_nav_menu( array(
			        'menu'              => 'primary',
			        'theme_location'    => 'primary',
			        'depth'             => 5,
			        'container'         => 'div',
			        'container_class'   => 'header-menu',
			        'container_id'      => '',
			        'menu_class'        => 'nav-menu',
			        'fallback_cb'       => '',
			        'walker'            => ''
                )
	        );
	        ?>
            <form role="search" method="get" id="searchform" action="http://ecodocx-new-wordpress.ecodocx.net">
                <input type="text" value="" name="s" id="s" placeholder="Search">
                <button type="submit" id="searchsubmit" value="Search">
                    <i class="fa fa-search"></i>
                </button>
            </form>
        </nav>

    </div>
</header>
<?php
wp_nav_menu( array(
		'menu'              => 'ecodocx-menu-small',
		'theme_location'    => 'primary',
		'depth'             => 5,
		'container'         => 'div',
		'container_class'   => 'header-menu-small',
		'container_id'      => '',
		'menu_class'        => 'nav-menu-small',
		'fallback_cb'       => '',
		'walker'            => ''
	)
);
?>
