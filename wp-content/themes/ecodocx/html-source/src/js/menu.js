/*scroll menu*/
var num = 400;
$(window).bind('scroll', function () {
    if ($(window).scrollTop() > num) {
        $('header').addClass('menu-reduced');
    } else {
        $('header').removeClass('menu-reduced');
    }
});

/* mobile menu */
jQuery(document).ready(function($){
    $("#burger-menu").on("click", function(){
        $(".mobile-menu").slideToggle();
        $(this).toggleClass("active");
    });
    $(".sub").on("click", function(){
        $(".mobile-menu-dropdown").slideToggle();
        $(this).toggleClass("active");
    });
});

/* slow */

