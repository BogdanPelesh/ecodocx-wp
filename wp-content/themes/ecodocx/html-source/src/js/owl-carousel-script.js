$(document).ready(function() {
    $("#slider").owlCarousel({
        loop: true,
        items: 1,
        nav: false,
        dots: true,
        autoplay:true,
        autoplayTimeout: 2500,
        autoplayHoverPause:true
    });
    $(".single-review").owlCarousel({
        loop: true,
        items: 1,
        nav: true,
        dots: false,
        navText: ["<i class=\"fa fa-angle-left prev\"></i>","<i class=\"fa fa-angle-right next\"></i>"]
    });

    $(".related-products").owlCarousel({
        loop: true,
        items: 1,
        nav: true,
        dots: false,
        navText: ["<i class=\"fa fa-angle-left prev\"></i>","<i class=\"fa fa-angle-right next\"></i>"]
    });
});
