/*
$(document).ready(function(){
    $('.container').each(function(){
        var highestBox = 0;
        $('.col-sm-6 ', this).each(function(){
            if($(this).height() > highestBox) {
                highestBox = $(this).height();
            }
        });
        $('.col-sm-6 ',this).height(highestBox);
    });
});
*/


$(window).scroll(function() {
    var mainParallax = $(this).scrollTop();
    $('.main-header-content').css({
        "transform" : "translate(0%, " + mainParallax /2 + "%"
    });
});