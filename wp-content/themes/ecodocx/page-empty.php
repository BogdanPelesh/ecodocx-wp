<?php
/*
Template Name: Page Empty
*/
?>

<?php if (get_bloginfo('language')=="de-DE") { ?>
	<?php get_header('de'); ?>
<?php } else if (get_bloginfo('language')=="en-US") { ?>
	<?php get_header(); ?>
<?php } ?>

<div id="markups">
    <a id="scroll-up"><i class="fa fa-arrow-up"></i></a>
    <a id="contact-us" href=call:+16174751636"><i class="fa fa-phone"></i></a>
    <a target="_blank" rel="nofollow" href="https://www.linkedin.com/company-beta/3565752/"><i class="fa fa-linkedin"></i></a>
    <a target="_blank" rel="nofollow" href="https://twitter.com/ecodocx/"><i class="fa fa-twitter"></i></a>
    <a target="_blank" rel="nofollow" href="https://www.facebook.com/ecodocx/"><i class="fa fa-facebook"></i></a>
    <a rel="nofollow" href="mailto:sales@ecodocx.com"><i class="fa fa-envelope"></i></a>
</div>

<?php if (is_page(array ('contact', 'contact-de'))) { ?>
<?php } else if (is_page()) { ?>
	<section id="breadcrumbs" style="background-color: #e1e8f6;" >
        <div class="container">
            <div class="row">
                <div class="col-md-12">
					<?php if ( function_exists('yoast_breadcrumb') )
					{yoast_breadcrumb('<div class="breadcrumbs">','</div>');} ?>
                </div>
            </div>
        </div>
    </section>
<?php } ?>

<?php the_post(); ?>
<?php the_content(); ?>

<?php if (get_bloginfo('language')=="de-DE") { ?>
	<?php get_footer('de'); ?>
<?php } else if (get_bloginfo('language')=="en-US") { ?>
	<?php get_footer(); ?>
<?php } ?>


