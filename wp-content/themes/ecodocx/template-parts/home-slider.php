<section id="slider" class="owl-carousel">
	<div class="single-slide">
		<div class="overlay">
			<img src="<?php echo get_theme_mod('slide-img-1', ''); ?>" alt="Image Slider 1">
		</div>
		<div class="slider-content">
			<div class="container">
				<h2><?php echo get_theme_mod('slide-title-1', ''); ?></h2>
				<p class="text-slider"><?php echo get_theme_mod('slide-text-1', ''); ?></p>
				<a class="btn-info" href="#">Learn more</a>
			</div>
		</div>
	</div>
	<div class="single-slide">
		<div class="overlay">
			<img src="<?php echo get_theme_mod('slide-img-2', ''); ?>" alt="Image Slider 2">
		</div>
		<div class="slider-content">
			<div class="container">
				<h2><?php echo get_theme_mod('slide-title-2', ''); ?></h2>
				<p class="text-slider"><?php echo get_theme_mod('slide-text-2', ''); ?></p>
				<a class="btn-info" href="#">Learn more</a>
			</div>
		</div>
	</div>
	<div class="single-slide">
		<div class="overlay">
			<img src="<?php echo get_theme_mod('slide-img-3', ''); ?>" alt="Image Slider 3">
		</div>
		<div class="slider-content">
			<div class="container">
				<h2><?php echo get_theme_mod('slide-title-3', ''); ?></h2>
				<p class="text-slider"><?php echo get_theme_mod('slide-text-3', ''); ?></p>
				<a class="btn-info" href="#">Learn more</a>
			</div>
		</div>
	</div>
</section>