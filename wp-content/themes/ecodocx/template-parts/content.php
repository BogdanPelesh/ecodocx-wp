<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ecodocx
 */

?>
<div class="col-sm-6 col-md-4">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header">
        <?php
        if ( has_post_thumbnail()) { ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
		        <?php the_post_thumbnail('medium'); ?>
            </a>
        <?php }
		if ( is_singular() ) :
			the_title( '<h4 class="entry-title">', '</h4>' );
		else :
			the_title( '<h4 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php /*ecodocx_posted_on(); */?>
            <div class="post-date"><i class="fa fa-calendar-check-o"></i><?php the_time('F j, Y'); ?></div>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</div><!-- .entry-header -->

	<div class="entry-content">
        <?php the_excerpt(); ?>
<!--		--><?php
//			the_content( sprintf(
//				wp_kses(
//					/* translators: %s: Name of current post. Only visible to screen readers */
//					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'ecodocx' ),
//					array(
//						'span' => array(
//							'class' => array(),
//						),
//					)
//				),
//				get_the_title()
//			) );
//
//			wp_link_pages( array(
//				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'ecodocx' ),
//				'after'  => '</div>',
//			) );
//		?>
	</div><!-- .entry-content -->


		<?php ecodocx_entry_footer(); ?>
	<!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
</div>
