<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ecodocx
 */

get_header(); ?>
    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
		            <?php if ( function_exists('yoast_breadcrumb') )
		            {yoast_breadcrumb('<div class="breadcrumbs">','</div>');} ?>
                </div>
            </div>
        </div>
    </section>
    <?php
    $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
    ?>
    <section class="post-header" class="parallax-window full" data-parallax="scroll" data-image-src="<?php the_post_thumbnail_url(); ?>">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="post-header-title">
                        <h1><?php echo CFS()->get( 'main_header_title_h1' ); ?></h1>
                        <h3><?php echo CFS()->get( 'main_header_title_h3' ); ?></h3>
                        <p><?php echo CFS()->get( 'main_header_text' ); ?></p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="post-header-content">
                        <div class="post-author">By <?php the_author(); ?></div>
                        <div class="post-date"><?php the_time('j F, Y'); ?></div>
                        <div class="share-button"></div>
                        <?php the_category(); ?>
                </div>
            </div>
        </div>
    </section>
    <div id="markups">
        <a id="scroll-up"><i class="fa fa-arrow-up"></i></a>
        <a id="contact-us" href=call:+16174751636"><i class="fa fa-phone"></i></a>
        <a target="_blank" rel="nofollow" href="https://www.linkedin.com/company-beta/3565752/"><i class="fa fa-linkedin"></i></a>
        <a target="_blank" rel="nofollow" href="https://twitter.com/ecodocx/"><i class="fa fa-twitter"></i></a>
        <a target="_blank" rel="nofollow" href="https://www.facebook.com/ecodocx/"><i class="fa fa-facebook"></i></a>
        <a rel="nofollow" href="mailto:sales@ecodocx.com"><i class="fa fa-envelope"></i></a>
    </div>
    <section class="post-page">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
	                <?php
	                while ( have_posts() ) : the_post();

		                get_template_part( 'template-parts/content-page', get_post_type() );

	                endwhile; // End of the loop.
	                ?>
                </div>
                <div class="col-md-3">
	                <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
                        <?php dynamic_sidebar( 'sidebar-1' ); ?>
	                <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
