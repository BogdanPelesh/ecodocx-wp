<?php
/**
 * @package Posts_in_Page
 * @author Eric Amundson <eric@ivycat.com>
 * @copyright Copyright (c) 2017, IvyCat, Inc.
 * @license http://www.gnu.org/licenses/gpl-2.0.html
 */ 
?>

<!-- NOTE: If you need to make changes to this file, copy it to your current theme's main
	directory so your changes won't be overwritten when the plugin is upgraded. -->

<!-- Post Wrap Start-->
<div class="col-slider-blog">
<div class="box">

<div class="box-img">
	<?php the_post_thumbnail('medium_large'); ?>
</div>
<div class="box-title">
	<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
</div>
<a href="<?php the_permalink(); ?>" class="cover-link" target="_blank"></a>
</div>
</div>

<!-- // Post Wrap End -->
