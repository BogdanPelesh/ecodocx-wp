<?php
/*
    Plugin Name: Image Upload Footer No title
    Plugin URI: http://wpshed.com/
    Description: Image Upload Footer No title from Ecodocx.
    Author: BP
    Author URI:
    Version: 0.1
*/



/**
 * Load scripts.
 */
function wpshed_image_upload_scripts_footer_notitle() {

    global $pagenow, $wp_customize;

    if ( 'widgets.php' === $pagenow || isset( $wp_customize ) ) {

        wp_enqueue_media();
        wp_enqueue_script( 'wpshed-image-upload', trailingslashit( plugin_dir_url( __FILE__ ) ) . 'js/upload.js', array( 'jquery' ) );
        wp_enqueue_style( 'wpshed-image-upload',  trailingslashit( plugin_dir_url( __FILE__ ) )  . 'css/upload.css' );

    }

}
add_action( 'admin_enqueue_scripts', 'wpshed_image_upload_scripts_footer_notitle' );


/**
 * Image Upload Widget
 */
class WPshed_Image_Upload_Widget_Footer_Notitle extends WP_Widget {

    // Holds widget settings defaults, populated in constructor.
    protected $defaults;

    // Constructor. Set the default widget options and create widget.
    function __construct() {

        $this->defaults = array(
            'title' => '',
            'text' => '',
            'image' => '',
            'link'  => '',
        );

        $widget_ops = array(
            'classname'   => 'wpshed-media-widget-footer-notitle',
            'description' => __( 'Image Upload from Ecodocx', 'wpshed' ),
        );

        $control_ops = array(
            'id_base' => 'wpshed-media-widget-footer-notitle',
            'width'   => 262,
            'height'  => 262,
        );

        parent::__construct( 'wpshed-media-widget-footer-notitle', __( 'Image Upload Footer No Title', 'wpshed' ), $widget_ops, $control_ops );

    }

    // The widget content.
    function widget( $args, $instance ) {

        //* Merge with defaults
        $instance = wp_parse_args( (array) $instance, $this->defaults );

        echo $args['before_widget']; ?>

        <div class="sidebar-wrap-ftr">
            <div class="image-wrap-ftr">
                <?php echo ( ! empty( $instance['image'] ) ) ? '<img src="' . $instance['image'] . '" alt="" />' : ''; ?>
            </div>
            <div class="widget_content">
                <?php echo ( ! empty( $instance['link'] ) ) ? '<a href="' . $instance['link'] . '">' : '';
                echo ( ! empty( $instance['text'] ) ) ? $args['before_text'] . $instance['text'] . $args['after_text'] : '';
                echo ( ! empty( $instance['link'] ) ) ? '</a>' : '';
                ?>
            </div>
	        <?php echo ( ! empty( $instance['link'] ) ) ? '<a href="' . $instance['link'] . '" class="cover-link">' : '';
	        echo ( ! empty( $instance['link'] ) ) ? '</a>' : ''; ?>
        </div>
        <?php
        echo $args['after_widget'];

    }

    // Update a particular instance.
    function update( $new_instance, $old_instance ) {

	    $new_instance['text']  = strip_tags( $new_instance['text'] );
        $new_instance['image']  = strip_tags( $new_instance['image'] );
        $new_instance['link']   = strip_tags( $new_instance['link'] );

        return $new_instance;

    }

    // The settings update form.
    function form( $instance ) {

        // Merge with defaults
        $instance = wp_parse_args( (array) $instance, $this->defaults );

        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'Text', 'wpshed' ); ?>:</label>
            <input type="text" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>" value="<?php echo esc_attr( $instance['text'] ); ?>" class="widefat" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'image' ); ?>"><?php _e( 'Image', 'wpshed' ); ?>:</label>
            <div class="wpshed-media-container">
                <div class="wpshed-media-inner">
                    <?php $img_style = ( $instance[ 'image' ] != '' ) ? '' : 'style="display:none;"'; ?>
                    <img id="<?php echo $this->get_field_id( 'image' ); ?>-preview" src="<?php echo esc_attr( $instance['image'] ); ?>" <?php echo $img_style; ?> />
                    <?php $no_img_style = ( $instance[ 'image' ] != '' ) ? 'style="display:none;"' : ''; ?>
                    <span class="wpshed-no-image" id="<?php echo $this->get_field_id( 'image' ); ?>-noimg" <?php echo $no_img_style; ?>><?php _e( 'No image selected', 'wpshed' ); ?></span>
                </div>
            
            <input type="text" id="<?php echo $this->get_field_id( 'image' ); ?>" name="<?php echo $this->get_field_name( 'image' ); ?>" value="<?php echo esc_attr( $instance['image'] ); ?>" class="wpshed-media-url" />

            <input type="button" value="<?php echo _e( 'Remove', 'wpshed' ); ?>" class="button wpshed-media-remove" id="<?php echo $this->get_field_id( 'image' ); ?>-remove" <?php echo $img_style; ?> />

            <?php $button_text = ( $instance[ 'image' ] != '' ) ? __( 'Change Image', 'wpshed' ) : __( 'Select Image', 'wpshed' ); ?>
            <input type="button" value="<?php echo $button_text; ?>" class="button wpshed-media-upload" id="<?php echo $this->get_field_id( 'image' ); ?>-button" />
            <br class="clear">
            </div>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e( 'URL', 'wpshed' ); ?>:</label>
            <input type="text" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" value="<?php echo esc_attr( $instance['link'] ); ?>" class="widefat" />
        </p>
        <?php

    }

}


/**
 * Register Widget
 */
function register_wpshed_image_upload_widget_footer_notitle() {
  
    register_widget( 'WPshed_Image_Upload_Widget_Footer_Notitle' );

} 
add_action( 'widgets_init','register_wpshed_image_upload_widget_footer_notitle' );
