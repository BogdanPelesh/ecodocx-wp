=== Wordpress Widget - Image Upload ===
Contributors: Holger Deschakovski
Tags: image upload, upload, host, hosting, photo, photos, foto, fotos, share, sharing, free image host, picture hosting, photo hosting, gallerys, images upload, photos storage, myspace, ebay, facebook, hi5, bebo, xanga, orkut, livejournal, friendsterm, badoo, hochladen, photo album, fotoalbum, fotobuch, upload, auktion, bilder, online, bilder hosting, bild
Requires at least: 2.0
Tested up to: 4.8

Display an Image Upload Box from photoalbum-2day.com.

== Description ==

Offer an Image Upload Box from photoalbum-2day.com.
Multiple Image Uploads, set Size of Thumbnails,
easy to use HTML Codes for displaying your Images 
in Websites, Emails, Wordpress Blogs.
Cloud Hosting for your Images with unlimited Bandwith & Storage.
More Information at http://www.photoalbum-2day.com.

If you like this Image Host Plugin please rate it https://wordpress.org/plugins/image-upload/.
Thank you :)


== Installation ==

1. Upload to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in Network Admin
3. Put the Widget into your Template at Admin Menu -> Appearance -> Widgets

