// JavaScript Document

function getBaseURL () {
   return location.protocol + '//' + location.hostname + 
      (location.port && ':' + location.port) + '/';
}

(function() {
    tinymce.create('tinymce.plugins.vecb_button4', {
        init : function(ed, url) {
            ed.addButton('vecb_button4', {
                title : '4 column white',image : 'http://ecodocx-new-wordpress.ecodocx.net/wp-content/uploads/vecb/column-4.png',onclick : function() {
                     ed.selection.setContent('<section class="advantages"><div class="container"><div class="row"><h2>Flexible and effective solution for:</h2><div class="col-xs-12 col-sm-6 col-md-3"><div class="advantage-item"><div class="icon anBottom"><img src="http://via.placeholder.com/100x100" alt="" width="100" height="100" class="alignnone size-full wp-image-618" /></div><h4>Lorem Ipsum</h4><p><b>Lorem ipsum</b> dolor sit amet, consectetur adipiscing elit</p></div></div><div class="col-xs-12 col-sm-6 col-md-3"><div class="advantage-item"><div class="icon anBottom"><img src="http://via.placeholder.com/100x100" alt="" width="100" height="100" class="alignnone size-full wp-image-618" /></div><h4>Lorem Ipsum</h4><p><b>Lorem ipsum</b> dolor sit amet, consectetur adipiscing elit</p></div></div><div class="col-xs-12 col-sm-6 col-md-3"><div class="advantage-item"><div class="icon anBottom"><img src="http://via.placeholder.com/100x100" alt="" width="100" height="100" class="alignnone size-full wp-image-618" /></div><h4>Lorem Ipsum</h4><p><b>Lorem ipsum</b> dolor sit amet, consectetur adipiscing elit</p></div></div><div class="col-xs-12 col-sm-6 col-md-3"><div class="advantage-item"><div class="icon anBottom"><img src="http://via.placeholder.com/100x100" alt="" width="100" height="100" class="alignnone size-full wp-image-618" /></div><h4>Lorem Ipsum</h4><p><b>Lorem ipsum</b> dolor sit amet, consectetur adipiscing elit</p></div></div></div></div></section>');
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('vecb_button4', tinymce.plugins.vecb_button4);
})();