// JavaScript Document

function getBaseURL () {
   return location.protocol + '//' + location.hostname + 
      (location.port && ':' + location.port) + '/';
}

(function() {
    tinymce.create('tinymce.plugins.vecb_button9', {
        init : function(ed, url) {
            ed.addButton('vecb_button9', {
                title : 'btn-primary',image : 'http://ecodocx-new-wordpress.ecodocx.net/wp-content/uploads/vecb/btn-primary.png',onclick : function() {
                     ed.selection.setContent('<a href="#" class="btn-primary">Learn more</a>');
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('vecb_button9', tinymce.plugins.vecb_button9);
})();