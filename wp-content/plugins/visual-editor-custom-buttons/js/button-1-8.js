// JavaScript Document

function getBaseURL () {
   return location.protocol + '//' + location.hostname + 
      (location.port && ':' + location.port) + '/';
}

(function() {
    tinymce.create('tinymce.plugins.vecb_button8', {
        init : function(ed, url) {
            ed.addButton('vecb_button8', {
                title : 'btn-info',image : 'http://ecodocx-new-wordpress.ecodocx.net/wp-content/uploads/vecb/btn-info.png',onclick : function() {
                     ed.selection.setContent('<a href="#" class="btn-info">Learn more</a>');
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('vecb_button8', tinymce.plugins.vecb_button8);
})();