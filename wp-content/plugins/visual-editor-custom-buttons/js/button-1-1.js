// JavaScript Document

function getBaseURL () {
   return location.protocol + '//' + location.hostname + 
      (location.port && ':' + location.port) + '/';
}

(function() {
    tinymce.create('tinymce.plugins.vecb_button1', {
        init : function(ed, url) {
            ed.addButton('vecb_button1', {
                title : '1 column white',image : 'http://ecodocx-new-wordpress.ecodocx.net/wp-content/uploads/vecb/column-1.png',onclick : function() {
                     ed.selection.setContent('<section class="info-product"><div class="container"><div class="row"><h2>Title</h2><div class="col-xs-12"><div class="description"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p></div></div><div class="text-center col-xs-12"><a class="btn btn-custom" href="/download-brochure/">Download brochure</a> <a class="btn" href="/request-demo/">Request a demo</a></div></div></div></section>');
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('vecb_button1', tinymce.plugins.vecb_button1);
})();