// JavaScript Document

function getBaseURL () {
   return location.protocol + '//' + location.hostname + 
      (location.port && ':' + location.port) + '/';
}

(function() {
    tinymce.create('tinymce.plugins.vecb_button7', {
        init : function(ed, url) {
            ed.addButton('vecb_button7', {
                title : 'btn',image : 'http://ecodocx-new-wordpress.ecodocx.net/wp-content/uploads/vecb/btn.png',onclick : function() {
                     ed.selection.setContent('<a href="#" class="btn">Learn more</a>');
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('vecb_button7', tinymce.plugins.vecb_button7);
})();