// JavaScript Document

function getBaseURL () {
   return location.protocol + '//' + location.hostname + 
      (location.port && ':' + location.port) + '/';
}

(function() {
    tinymce.create('tinymce.plugins.vecb_button6', {
        init : function(ed, url) {
            ed.addButton('vecb_button6', {
                title : '2 column grey',image : 'http://ecodocx-new-wordpress.ecodocx.net/wp-content/uploads/vecb/column-2-grey.png',onclick : function() {
                     ed.selection.setContent('<section class="content-block bg-caramel"><h2 class="blink">Content Server is designed to help your<br /> employees to:</h2><div class="container"><div class="row"><div class="col-xs-12 col-sm-6 col-sm-push-6"><img src="http://via.placeholder.com/530x370" alt="" /></div><div class="col-xs-12 col-sm-6 col-sm-pull-6"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p><a class="btn" href="/request-demo/">Request a demo</a></div></div></div></section>');
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('vecb_button6', tinymce.plugins.vecb_button6);
})();